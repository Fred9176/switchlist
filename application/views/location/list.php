<?php $this->utilities->display_flashdata(); ?>

<p><a href="<?= site_url('location/create') ?>"><?= $this->lang->line('location_add_new'); ?></a></p>

<table width="600" cellpadding="5" class="table table-hover table-striped table-custom-auto-width">
    <thead>
        <tr>
            <th class="text-center" scope="col"><?= $this->lang->line('order'); ?></th>
            <th scope="col"><?= $this->lang->line('name'); ?></th>
            <th class="text-center" scope="col"><?= $this->lang->line('actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($locations as $location) : ?>
            <tr>
                <td class="text-center"><?= $location->order ?></td>
                <td><?= $location->name ?></td>
                <td class="text-center">
                    <a href="<?= site_url("location/edit/" . $location->id) ?>" class="btn btn-success"><i class="bi-pencil"></i></a>&nbsp;
                    <a href="<?= site_url("location/delete/" . $location->id) ?>" data-confirm="<?= $this->lang->line('are_you_sure'); ?>" class="btn btn-danger"><i class="bi-trash"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?= $pagination ?>