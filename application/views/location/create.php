<?php $this->utilities->display_flashdata(); ?>

<form action="" method="post">
    <?php
    $this->load->view("location/_form");
    ?>
    <p>
        <input type="submit" class="btn btn-primary" name="create" value="<?= $this->lang->line('add'); ?>" />
    </p>
</form>