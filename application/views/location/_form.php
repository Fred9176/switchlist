<h3><?= $this->lang->line('location_information'); ?></h3>
<!-- Name -->
<div class="form-group">
    <label class="control-label col-sm-2" for="location[name]"><?= $this->lang->line('name'); ?>:</label>
    <div class="col-sm-10">
        <?= form_input(array('name' => 'location[name]', 'value' => $location->name, 'class' => 'form-control')); ?>
    </div>
</div>

<!-- Order -->
<div class="form-group">
    <label class="control-label col-sm-2" for="location[order]"><?= $this->lang->line('order'); ?>:</label>
    <div class="col-sm-10">
        <?= form_input(array('name' => 'location[order]', 'value' => $location->order, 'class' => 'form-control')); ?>
    </div>
</div>