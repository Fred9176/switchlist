<?php $this->utilities->display_flashdata(); ?>

<p><a href="<?= site_url('industry/create') ?>"><?= $this->lang->line('industry_add_new'); ?></a></p>

<table width="600" cellpadding="5" class="table table-hover table-striped table-custom-auto-width">
    <thead>
        <tr>
            <th scope="col"><?= $this->lang->line('location'); ?></th>
            <th scope="col"><?= $this->lang->line('name'); ?></th>
            <th class="text-center" scope="col"><?= $this->lang->line('size'); ?></th>
            <th class="text-center" scope="col"><?= $this->lang->line('allowed_cars'); ?></th>
            <th class="text-center" scope="col"><?= $this->lang->line('actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($industries as $industry) : ?>
            <tr>
                <td><?= $industry->location_name; ?></td>
                <td><?= $industry->name; ?></td>
                <td class="text-center"><?= $industry->size; ?></td>
                <td>
                    <?php
                    $allowed_cars = implode(', ', array_values($industry->allowed_cars));
                    echo $allowed_cars == '' ? $this->lang->line('all') : $allowed_cars;
                    ?>
                </td>
                <td class="text-center">
                    <a href="<?= site_url("industry/edit/" . $industry->id) ?>" class="btn btn-success"><i class="bi-pencil"></i></a>&nbsp;
                    <a href="<?= site_url("industry/delete/" . $industry->id) ?>" data-confirm="<?= $this->lang->line('are_you_sure'); ?>" class="btn btn-danger"><i class="bi-trash"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?= $pagination ?>