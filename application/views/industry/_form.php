<h3><?= $this->lang->line('industry_information'); ?></h3>
<!-- Name -->
<div class="form-group">
    <label class="control-label col-sm-2" for="industry[name]"><?= $this->lang->line('name'); ?>:</label>
    <div class="col-sm-10">
        <?= form_input(array('name' => 'industry[name]', 'value' => $industry->name, 'class' => 'form-control')); ?>
    </div>
</div>

<!-- Location -->
<div class="form-group">
    <label class="control-label col-sm-2" for="industry[location_id]"><?= $this->lang->line('location'); ?>:</label>
    <div class="col-sm-10">
        <?= form_dropdown('industry[location_id]', $locations, $industry->location_id, array('class' => 'form-control')); ?>
    </div>
</div>

<!-- Size -->
<div class="form-group">
    <label class="control-label col-sm-2" for="industry[size]"><?= $this->lang->line('size'); ?>:</label>
    <div class="col-sm-10">
        <?= form_input(array('name' => 'industry[size]', 'value' => $industry->size, 'class' => 'form-control')); ?>
    </div>
</div>

<h3><?= $this->lang->line('allowed_cars'); ?></h3>
<?php

foreach ($cars as $car) :

?>
    <div class="checkbox">
        <?php
        echo form_checkbox(array(
            'name' => 'allowedCars[]',
            'value' => $car->id,
            'checked' => array_search($car->id, $allowedCars) !== false,
            'id' => 'allowedCars' . $car->id
        )); ?>
        <label for="<?= 'allowedCars' . $car->id; ?>"><?= $car->type; ?></label>
    </div>
<?php endforeach; ?>