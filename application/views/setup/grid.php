<?php $this->utilities->display_flashdata(); ?>

<form method="post" class="row row-cols-lg-auto g-3 align-items-center">
    <div class="col-12">
        <label for="content"><?= str_replace(' ', '&nbsp;', $this->lang->line('setup_number_of_cars_to_spot', [$cars_count])); ?>:</label>
    </div>
    <div class="col-12">
        <?= form_input(['name' => 'number_to_spot', 'class' => 'form-control input-numeric']); ?>
    </div>
    <div class="col-12">
        <input type="submit" class="btn btn-primary" name="get_random" value="<?= $this->lang->line('setup_generate_random_spots'); ?>" />
    </div>
</form>
<hr>
<form action="" method="post">
    <table class="table table-bordered table-hover table-sm table-custom-fit">

        <?php
        $prev_location = '';
        foreach ($industries as $industry) :
            // New header if we are on new location
            if ($industry->location_name != $prev_location) {
        ?>
                <thead class="table-primary">
                    <tr>
                        <th scope="col"><?= $industry->location_name; ?></th>
                        <?php foreach ($cars as $car) : ?>
                            <th class="text-center" scope="col"><?= $car->type; ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
            <?php

            }
            $prev_location = $industry->location_name;
            ?>
            <tr>
                <td><?= $industry->name ?></td>

                <?php foreach ($cars as $car) : ?>

                    <td class="text-center">

                        <span class="stepper">
                            <button type="button" class="btn btn-outline-danger">–</button>
                            <?php

                            $number_of_cars = isset($spotted_cars[$industry->id]) ? count(array_keys($spotted_cars[$industry->id], $car->id)) : 0;
                            $value = $number_of_cars > 0 ? $number_of_cars : '';

                            echo form_input([
                                'name' => 'spotted_cars[' . $industry->id . '][' . $car->id . ']',
                                'id' => 'spotted_cars_' . $industry->id . '_' . $car->id,
                                'value' => $value,
                                //'type' => 'number',
                                'class' => '',
                                'min' => 0,
                                'max' => 1000,
                                'step' => 1,
                            ]);
                            ?>
                            <buttontype="button" class="btn btn-outline-success">+</button>
                        </span>
                    </td>

                <?php endforeach; ?>

            </tr>
        <?php endforeach; ?>

    </table>
    <p>
        <input type="submit" class="btn btn-primary" name="save" value="<?= $this->lang->line('save'); ?>" />
    </p>
</form>