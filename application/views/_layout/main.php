<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?= base_url('/assets/img/favicon/apple-touch-icon-57x57.png'); ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= base_url('/assets/img/favicon/apple-touch-icon-114x114.png'); ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url('/assets/img/favicon/apple-touch-icon-72x72.png'); ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url('/assets/img/favicon/apple-touch-icon-144x144.png'); ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?= base_url('/assets/img/favicon/apple-touch-icon-60x60.png'); ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?= base_url('/assets/img/favicon/apple-touch-icon-120x120.png'); ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?= base_url('/assets/img/favicon/apple-touch-icon-76x76.png'); ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?= base_url('/assets/img/favicon/apple-touch-icon-152x152.png'); ?>" />
    <link rel="icon" type="image/png" href="<?= base_url('/assets/img/favicon/favicon-196x196.png'); ?>" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?= base_url('/assets/img/favicon/favicon-96x96.png'); ?>" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?= base_url('/assets/img/favicon/favicon-32x32.png'); ?>" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?= base_url('/assets/img/favicon/favicon-16x16.png'); ?>" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?= base_url('/assets/img/favicon/favicon-128.png'); ?>" sizes="128x128" />
    <meta name="application-name" content="Switchlist" />
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?= base_url('/assets/img/favicon/mstile-144x144.png'); ?>" />
    <meta name="msapplication-square70x70logo" content="<?= base_url('/assets/img/favicon/mstile-70x70.png'); ?>" />
    <meta name="msapplication-square150x150logo" content="<?= base_url('/assets/img/favicon/mstile-150x150.png'); ?>" />
    <meta name="msapplication-wide310x150logo" content="<?= base_url('/assets/img/favicon/mstile-310x150.png'); ?>" />
    <meta name="msapplication-square310x310logo" content="<?= base_url('/assets/img/favicon/mstile-310x310.png'); ?>" />


    <title>Switchlist - <?= $title; ?></title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="<?= base_url('/assets/css/styles.css'); ?>" rel="stylesheet" media="screen">


    <?= $layout_css; ?>
    <?= $layout_js_head; ?>

</head>

<body>
    <!-- Nav bar -->
    <?= $navbar; ?>

    <div class="container">
        <!-- Title -->
        <h1><?= $title; ?></h1>
        <hr>

        <!-- Main content -->
        <?= $layout_content; ?>
    </div>

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Other scripts (jquery is no more needed with Bootstrap 5) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="<?= base_url('/assets/js/functions.js'); ?>"></script>
    <?= $layout_js_bottom; ?>

</body>

</html>