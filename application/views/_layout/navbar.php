<?php
$this->config->load('_navbar');
$navbar_items = $this->config->item('navbar_items');
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top py-0">
    <div class="container-fluid">
        <a class="navbar-brand" href="<?= base_url('/'); ?>">Switchlist</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav mr-auto">
                <?php
                foreach ($navbar_items as $item_name => $item_datas) {
                    $class_active = ($navbar_active == $item_name ? ' active' : '');

                    // Main items with sub items
                    if (isset($item_datas['subitems'])) {
                        ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link <?= $class_active; ?> dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?= isset($item_datas['icon']) ? '<i class="bi-' . $item_datas['icon'] . '"></i>&nbsp;' : ''; ?>
                                <?= $item_datas['text']; ?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <?php
                                foreach ($item_datas['subitems'] as $subitem => $subitem_datas) {

                                    if (isset($subitem_datas['type']) && $subitem_datas['type'] == 'separator') {

                                        echo '<a class="dropdown-item" href="#">&nbsp;</a>';
                                    } else {

                                        //var_dump($subitem_datas);
                                        $link = isset($subitem_datas['external']) && $subitem_datas['external'] ? site_url('iframe') . '/?title=' . urlencode($subitem_datas['text']) . '&link=' . urlencode($subitem_datas['link']) : site_url($subitem_datas['link']);
                                        echo '<a class="dropdown-item" href="' . $link . '">';
                                        echo isset($subitem_datas['icon']) ? '<i class="bi-' . $subitem_datas['icon'] . '"></i>&nbsp;' : '';
                                        echo $subitem_datas['text'];
                                        echo '</a>';
                                    }
                                }
                                ?>
                            </div>
                        </li>
                        <?php
                    } else {
                        $link = isset($item_datas['external']) && $item_datas['external'] ? site_url('iframe') . '/?title=' . urlencode($item_datas['text']) . '&link=' . urlencode($item_datas['link']) : site_url($item_datas['link']);
                        ?>
                        <li class="nav-item">
                            <a class="nav-link<?= $class_active; ?>" href="<?= $link; ?>">
                                <?= isset($item_datas['icon']) ? '<i class="bi-' . $item_datas['icon'] . '"></i>&nbsp;' : ''; ?>
                                <?= $item_datas['text']; ?>
                            </a>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</nav>