<?php $this->utilities->display_flashdata(); ?>

<form action="" method="post" class="form-horizontal" role="form">
    <?php
    $this->load->view("car/_form");
    ?>
    <p>
        <input type="submit" class="btn btn-primary" name="create_car" value="<?= $this->lang->line('add'); ?>" />
    </p>
</form>