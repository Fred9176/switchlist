<?php $this->utilities->display_flashdata(); ?>

<form action="" method="post">
    <?php
    $this->load->view("car/_form");
    ?>
    <p>
        <input type="submit" class="btn btn-primary" name="update_car" value="<?= $this->lang->line('update'); ?>" />
    </p>
</form>