<?php $this->utilities->display_flashdata(); ?>

<p><a href="<?= site_url('car/create') ?>"><?= $this->lang->line('car_add_new'); ?></a></p>

<table class="table table-hover table-striped table-custom-auto-width">
    <thead>
        <tr>
            <th scope="col"><?= $this->lang->line('type'); ?></th>
            <th class="text-center" scope="col"><?= $this->lang->line('owned'); ?></th>
            <th class="text-center" scope="col"><?= $this->lang->line('actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1;
        foreach ($cars as $car) : ?>
            <tr>
                <td><?= $car->type ?></td>
                <td class="text-center"><?= $car->number ?></td>
                <td class="text-center">
                    <a href="<?= site_url("car/edit/" . $car->id) ?>" class="btn btn-success"><i class="bi-pencil"></i></a>&nbsp;
                    <a href="<?= site_url("car/delete/" . $car->id) ?>" data-confirm="<?= $this->lang->line('are_you_sure'); ?>" class="btn btn-danger"><i class="bi-trash"></i></a>
                </td>
            </tr>
        <?php $i++;
        endforeach; ?>
    </tbody>
</table>
<?= $pagination ?>