<!-- Type -->
<div class="form-group">
    <label class="control-label col-sm-2" for="title"><?= $this->lang->line('type'); ?>:</label>
    <div class="col-sm-10">
        <?= form_input(array('name' => 'car[type]', 'value' => $car->type, 'class' => 'form-control')); ?>
    </div>
</div>

<!-- Owned -->
<div class="form-group">
    <label class="control-label col-sm-2" for="content"><?= $this->lang->line('owned'); ?>:</label>
    <div class="col-sm-10">
        <?= form_input(array('name' => 'car[number]', 'value' => $car->number, 'class' => 'form-control')); ?>
    </div>
</div>