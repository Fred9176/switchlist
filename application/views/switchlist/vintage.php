<!doctype html>

<html lang="en">

    <head>
        <meta charset="utf-8">

        <title>Switchlist</title>
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=1, height = device-height" />
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Oswald&family=Rock+Salt&family=Liu+Jian+Mao+Cao&family=Over+the+Rainbow&display=swap" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <style>
            @font-face {
                font-family: typewriter;
                /* src: url(fonts/HERMES1943.TTF); */
                /* src: url(fonts/Underwood_Etendu_1913.TTF); */
                src: url(/assets/fonts/uwch.ttf);
                /* src: url(fonts/JMHTypewriter.TTF); */
            }


            .handwriting {
                color: #2D3082;
                /* font-family: 'Liu Jian Mao Cao', cursive; */
                /* font-family: 'Over the Rainbow', cursive; */
                /* font-family: 'Oswald', sans-serif; */
                /* font-family: "Courier New", Courier, Prestige, monospace; */
                font-family: typewriter;
                font-size: 16px;
                text-transform: none;
            }



            /* Content */
            body {
                text-transform: uppercase;
                font-family: 'Oswald', sans-serif;
                /* background-image: url(/assets/img/switchlist/paper.jpg); */
                /* background-repeat: no-repeat; */
                /* background-size: 1100px; */

            }

            .content {
                background-color: #F0E9E1;
                max-width: 900px;
                margin: auto;
                padding: 30px;
                border: 1px solid black;
            }


            /* Header */
            .header-title {
                text-align: center;
            }

            h1 {
                text-align: center;
                margin: 0;
            }



            /* Table */

            table {
                width: 100%;
                border-collapse: collapse;
            }

            tr,
            td {
                border: 1px solid black;
            }


            td {
                padding: 5px;
            }

            td.center {
                text-align: center;
            }

            tr.noborder,
            tr.noborder td {
                border: 0;
            }

            /* Eastbound/Westbound header */
            .switchlist-direction td {
                text-align: center;
                padding-top: 25px;
                margin-bottom: 1em;
            }


            .switchlist-direction .handwriting {
                text-decoration: underline;
            }



            .switchlist-heading {
                background-color: #B8B8B8;
            }


            /* Checkbox */
            input[type=checkbox].css-checkbox {
                display: none;
            }

            input[type=checkbox].css-checkbox+label.css-label {
                padding-left: 25px;
                height: 18px;
                display: inline-block;
                line-height: 18px;
                background-repeat: no-repeat;
                background-position: 0 0;
                font-size: 18px;
                vertical-align: middle;
                cursor: pointer;

            }

            input[type=checkbox].css-checkbox:checked+label.css-label {
                background-position: 0 -18px;
            }

            label.css-label {
                background-image: url(/assets/img/switchlist/checkbox.png);
                -webkit-touch-callout: none;
                -webkit-user-select: none;
                -khtml-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }
        </style>
    </head>

    <body>
        <div class="content">
            <div class="header-title">Aylesbury & Wymore Railroad</div>
            <h1>SWITCH LIST</h1>
            <?php
            // No cars to move => display a warning
            if ($cars_to_move_count == 0) {
                echo $this->lang->line('switchlist_please_select_a_number_of_cars_to_move_above');
            }

            // Display the switchlist
            else {

                //$this->utilities->debug($trains, '', FALSE);

                $jobs = array('drop', 'pickup');

                echo '<table>';
                foreach ($trains as $train => $industries) {

                    //$this->utilities->debug($industries, '', FALSE);
                    $i = 0;
                    $location_id_prev = 0;

                    // Get first and last locations
                    $all_values = array_values($industries);

                    $first_location = $all_values[0]['location_name'];
                    $last_location = $all_values[count($all_values) - 1]['location_name'];
                    ?>

                    <tr class="switchlist-direction noborder">
                        <td colspan="5">
                            <?= $train; ?> train,
                            <span class="handwriting"><?= format_handwriting_fill($first_location); ?></span> To <span class="handwriting"><?= format_handwriting_fill($last_location); ?></span>,
                            on <span class="handwriting"><?= format_handwriting_fill($date['day'], 3); ?></span> 20<span class="handwriting"><?= format_handwriting_fill($date['year'], 3); ?></span>
                        </td>
                    </tr>
                    <?php
                    foreach ($industries as $industry_id => $industry_data) {
                        $i++;

                        // If there is any job to be done
                        if (count($industry_data['drop']) > 0 or count($industry_data['pickup']) > 0) {

                            // Start a new location block if location changed
                            if ($industry_data['location_id'] != $location_id_prev) {

                                echo '<tr class="switchlist-location noborder"><td colspan="4">' . $industry_data['location_name'] . '</h3></td>';
                                echo '<tr class="switchlist-heading">';
                                echo '<td></td>';
                                echo '<td>' . $this->lang->line('industry') . '</td>';
                                echo '<td>' . $this->lang->line('action') . '</td>';
                                echo '<td>' . $this->lang->line('from') . '</td>';
                                echo '<td>' . $this->lang->line('to') . '</td>';
                                echo '</tr>';
                            }


                            foreach ($jobs as $job) {

                                if (isset($industry_data[$job])) {
                                    $car_moves = $industry_data[$job];

                                    foreach ($car_moves as $id => $data) {
                                        //$checkbox_uuid = uniqid();
                                        $checkbox_id = $job . '_' . $id;
                                        ?>
                                        <tr class="switchlist-line handwriting">
                                            <td class="center">
                                                <input type="checkbox" id="<?= $checkbox_id; ?>" class="css-checkbox switchlist-move" name="move[<?= $id; ?>]" id="<?= $job . '_' . $id; ?>" value="<?= $id; ?>" <?= ($job == 'pickup' && $data['car_state'] != null) || ($job == 'drop' && $data['car_state'] == 'dropped') ? 'checked' : '' ?> />
                                                <label class="css-label" for="<?= $checkbox_id; ?>"></label>
                                            </td>
                                            <td><?= $industry_data['name']; ?></td>
                                            <td><?= $this->lang->line($job, [$data['car_type']]); ?></td>
                                            <td><?= $job == 'pickup' ? '&nbsp;' : $data['from']; ?></td>
                                            <td><?= $job == 'pickup' ? $data['move_to'] : '&nbsp;'; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }

                            // Previous location to see if the location has chaged (and thus display a new block)
                            $location_id_prev = $industry_data['location_id'];
                        }
                    }
                }
                echo '</table>';
            }
            ?>
        </div>

        <script type="text/javascript">
            //$(function () {
            /*
             * Save switchlist move
             */

            $('.switchlist-move').change(function () {

                // Get values related to the move
                var id = $(this).val();

                var pickup = $('#pickup_' + id).is(":checked");
                var drop = $('#drop_' + id).is(":checked");


                // Ajax post
                $.ajax({
                    type: 'POST',
                    url: '<?= site_url('switchlist/save_move') ?>',
                    dataType: 'json',
                    data: {
                        id: id,
                        pickup: pickup,
                        drop: drop
                    },
                    /*
                     success: function (res) {
                     if (res)
                     {
                     alert(res.id + ':' + res.pickup + '/' + res.drop + ' -> ' + res.state);
                     }
                     }*/
                });


            });
            //});
        </script>
    </body>

</html>