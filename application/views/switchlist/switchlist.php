<?php $this->utilities->display_flashdata(); ?>

<form action="<?= site_url('switchlist/generate'); ?>" method="post" class="row row-cols-lg-auto g-3 align-items-center">
    <div class="col-12">
        <label for="content"><?= str_replace(' ', '&nbsp;', $this->lang->line('switchlist_number_of_cars_to_move', [$spotted_cars_count])); ?>:</label>
    </div>
    <div class="col-12">
        <?= form_input(array('name' => 'number_to_move', 'class' => 'form-control input-numeric')); ?>
    </div>
    <div class="col-12">
        <input type="submit" class="btn btn-primary" name="generate_moves" value="<?= $this->lang->line('switchlist_generate_new_moves'); ?>" />
    </div>
</form>

<div id="result"></div>
<hr>
<div><a href="<?= site_url('switchlist/vintage'); ?>">Vintage view</a></div>
<?php
// No cars to move => display a warning
if ($cars_to_move_count == 0) {
    echo bootstrap_alert($this->lang->line('switchlist_please_select_a_number_of_cars_to_move_above'), 'warning');
}

// Display the switchlist
else {

    //$this->utilities->debug($trains, '',FALSE);


    $jobs = array('drop', 'pickup');

    foreach ($trains as $train => $industries) {


        echo '<h2>' . $train . '</h2>';

        $i = 0;
        $location_id_prev = 0;

        echo '<table class="table">';

        foreach ($industries as $industry_id => $industry_data) {
            $i++;

            // If there is any job to be done
            if (count($industry_data['drop']) > 0 or count($industry_data['pickup']) > 0) {

                // Start a new location block if location changed
                if ($industry_data['location_id'] != $location_id_prev) {

                    //echo $i > 1 ? '</tbody></table>' : '';

                    echo '<td colspan="5"><h3>' . $industry_data['location_name'] . '</h3></td>';
                    //echo '<thead>';
                    echo '<tr>';
                    echo '<th></th>';
                    echo '<th>' . $this->lang->line('industry') . '</th>';
                    echo '<th>' . $this->lang->line('action') . '</th>';
                    echo '<th>' . $this->lang->line('from') . '</th>';
                    echo '<th>' . $this->lang->line('to') . '</th>';
                    echo '</tr>';
                    //echo '</thead>';
                    //echo '<tbody>';
                }


                foreach ($jobs as $job) {

                    if (isset($industry_data[$job])) {
                        $car_moves = $industry_data[$job];

                        foreach ($car_moves as $id => $data) {
                            ?>
                            <tr>
                                <td class="fit">
                                    <div class="checkbox checkbox-<?= $job == 'pickup' ? 'info' : 'success'; ?>">
                                        <input type="checkbox" class="switchlist-move" name="move[<?= $id; ?>]" id="<?= $job . '_' . $id; ?>" value="<?= $id; ?>" <?= ($job == 'pickup' && $data['car_state'] != null) || ($job == 'drop' && $data['car_state'] == 'dropped') ? 'checked' : '' ?> />
                                        <label></label>
                                    </div>
                                </td>
                                <td><?= $industry_data['name']; ?></td>
                                <td><?= $this->lang->line($job, [$data['car_type']]); ?></td>
                                <td><?= $job == 'pickup' ? '&nbsp;' : $data['from']; ?></td>
                                <td><?= $job == 'pickup' ? $data['move_to'] : '&nbsp;'; ?></td>
                            </tr>
                            <?php
                        }
                    }
                }

                // Previous location to see if the location has chaged (and thus display a new block)
                $location_id_prev = $industry_data['location_id'];
            }
        }

        echo '</table>';
    }
}
?>
<script type="text/javascript">
    //$(function () {
    /*
     * Save switchlist move
     */

    $('.switchlist-move').change(function () {

        // Get values related to the move
        var id = $(this).val();

        var pickup = $('#pickup_' + id).is(":checked");
        var drop = $('#drop_' + id).is(":checked");


        // Ajax post
        $.ajax({
            type: 'POST',
            url: '<?= site_url('switchlist/save_move') ?>',
            dataType: 'json',
            data: {
                id: id,
                pickup: pickup,
                drop: drop
            },
            /*
             success: function (res) {
             if (res)
             {
             alert(res.id + ':' + res.pickup + '/' + res.drop + ' -> ' + res.state);
             }
             }*/
        });


    });
    //});
</script>