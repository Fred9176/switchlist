<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



if (!function_exists('format_handwriting_fill')) {

    function format_handwriting_fill($string, $rightpad = 5) {
        return str_repeat('&nbsp;', 1) . $string . str_repeat('&nbsp;', $rightpad);
    }

}