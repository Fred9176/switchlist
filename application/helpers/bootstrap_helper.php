<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



if (!function_exists('bootstrap_alert')) {
    /*
     * Generate an alert message
     * (generally used with session flash data)
     * 
     * @param string $message Message to display
     * @param string $state State of the message (success, info, alert, danger)
     * 
     * @return string html code to display the alert
     */
    function bootstrap_alert($message, $state = 'success')
    {
        if ($message != '') {
            return '<div class="alert alert-' . $state . '" role="alert">' . $message . '</div>';
        }
    }
}





if (!function_exists('bootstrap_pagination')) {

    /*
     * Generate pagination links
     * 
     * @param string $pagination CodeIgniter pagination object from pagination library
     * @param array $options Array of options
     * @param int $options|'total'] Total number of records
     * @param int $options|'page'] Current page
     * @param int $options|'limit'] Number of items per page
     * @param string $options|'base_url'] Base URL to create links
     * @param string $options|'config_*'] Parameter to override default pagination object (eg : first_tag_open)
     */
    function bootstrap_pagination($pagination, $options = [])
    {
        //$this->load->library('pagination');

        // Get specific options
        $total = isset($options['total']) ? $options['total'] : 0;
        $page = isset($options['page']) ? $options['page'] : 1;
        $limit = isset($options['limit']) ? $options['limit'] : 50;
        $offset = ($page - 1) * $limit;

        // Default values
        $global_tag_open = '<li>';
        $global_tag_close = '</li>';

        $config['base_url'] = $options['base_url'];
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $total;
        $config['per_page'] = $limit;
        $config['uri_segment'] = 2;
        $config['num_links'] = 5;

        $config['full_tag_open'] = '<nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = '&lt;&lt;';
        $config['last_link'] = '&gt;&gt;';
        $config['next_link'] = '&gt;';
        $config['prev_link'] = '&lt;';

        $config['first_tag_open'] = $global_tag_open;
        $config['first_tag_close'] = $global_tag_close;
        $config['last_tag_open'] = $global_tag_open;
        $config['last_tag_close'] = $global_tag_close;
        $config['prev_tag_open'] = $global_tag_open;
        $config['prev_tag_close'] = $global_tag_close;
        $config['next_tag_open'] = $global_tag_open;
        $config['next_tag_close'] = $global_tag_close;
        $config['num_tag_open'] = $global_tag_open;
        $config['num_tag_close'] = $global_tag_close;
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';


        // Override default configuration
        foreach ($options as $option => $value) {

            if (substr($option, 0, 7) == 'config_') {
                $config[substr($option, 7)] = $value;
            }
        }

        // Create pagination links
        $pagination->initialize($config);
        return [
            'limit' => $limit,
            'offset' => $offset,
            'links' => $pagination->create_links()
        ];
    }
}
