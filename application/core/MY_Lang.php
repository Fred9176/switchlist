<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * FProst : surcharge de la classe pour gérer les placeholders dans les chaînes
 * à traduire
 */

class MY_Lang extends CI_Lang {

    /**
     * Class constructor
     *
     * @return	void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Fetch a single line of text from the language array
     * Replace argument placeholders with values
     *
     * Usage:
     * # this msg is in your language file
     * $lang['hello_key'] = "Hello %s, you are visitor number %d";
     * ...
     * $this->lang->load('hello_language_file_name','english');
     * $msgvalues = array("Peter",21);
     * $msg = $this->lang->line('hello_key', $msgvalues);     
     *
     * @access    public
     * @param    string    the language line key
     * @param    mixed    argument or array of arguments
     * @return    string
     */
    function line($line_key = '', $args = '') {
        if ($line_key == '' OR ! isset($this->language[$line_key])) {
            return FALSE;
        }
        $line_string = $this->language[$line_key];
        if ($args == '') {
            return $line_string;
        }
        else if (is_array($args)) {
            return vsprintf($line_string, $args);
        }
        else {
            return sprintf($line_string, $args);
        }
    }

}
