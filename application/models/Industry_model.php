<?php

class Industry_model extends My_Model
{

    public $protected_attributes = ['id'];
    // Default value
    public $name = '';
    public $location_id = 0;
    public $size = 1;
    public $allowed_cars = [];
    // Validation rules
    public $validate = [
        [
            'field' => 'name',
            'label' => 'lang:name',
            'rules' => 'required'
        ],
        [
            'field' => 'location_id',
            'label' => 'lang:location',
            'rules' => 'required|greater_than[0]'
        ],
        [
            'field' => 'size',
            'label' => 'lang:size',
            'rules' => 'required|greater_than[0]'
        ],
    ];

    /*
     * get all industries with associated location ordred by location order
     */

    public function get_industries_with_location($params = [])
    {



        $this->db
            ->select('i.*, l.name as location_name')
            ->from('industries as i')
            ->join('locations as l', 'l.id = i.location_id');

        if (isset($params['pagination'])) {
            $this->db->limit($params['pagination']['limit'], $params['pagination']['offset']);
        }

        $this->db->order_by('l.order, i.name');

        return $this->db->get()->result();
    }
}
