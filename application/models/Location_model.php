<?php

class Location_model extends My_Model
{

    public $protected_attributes = ['id'];

    // Default value
    public $name = '';
    public $order = 1;

    // Validation rules
    public $validate = [
        [
            'field' => 'name',
            'label' => 'lang:name',
            'rules' => 'required'
        ],
        [
            'field' => 'order',
            'label' => 'lang:order',
            'rules' => 'required|greater_than[0]'
        ],
    ];

    /*
     * Retreive all locations in an array to be used in dropdown list
     */
    public function prepare_dropdown()
    {
        $data = [];
        $locations = $this->location->order_by('order, name')->get_all();
        foreach ($locations as $location) {
            $data[$location->id] = $location->name;
        }

        return $data;
    }
}
