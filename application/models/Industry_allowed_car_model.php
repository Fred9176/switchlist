<?php

class Industry_allowed_car_model extends My_Model
{


    /*
     * Retrieve carsids allowed for all or specified industry
     * 
     * @param int $industry_id Industry Id to retrieve
     * 
     * @return array List of industries containing array of car ids
     */
    public function get_car_ids($industry_id = NULL)
    {

        $result = is_null($industry_id) ? $this->get_all() :  $this->get_many_by(['industry_id' => $industry_id]);

        // We return only a list of ids
        $data = [];
        foreach ($result as $row) {

            // If no industry_id is specified, then we get list by industry
            if (is_null($industry_id)) {
                $data[$row->industry_id][] = $row->car_id;
            } else {
                $data[] = $row->car_id;
            }
        }

        return $data;
    }


    /*
     * Retrieve cars allowed for the specified industry
     * 
     * @param integer $industry_id Industry ID
     * @return array List of car ids and type
     */
    public function get_cars($industry_id, $get_ids = FALSE)
    {

        $result = $this->_database->select('*')
            ->from($this->_table)
            ->join('cars', 'cars.id = ' . $this->_table . '.car_id', 'inner')
            ->where(['industry_id' => $industry_id])
            ->order_by('cars.type')
            ->get()
            ->result();

        $data = [];
        foreach ($result as $row) {
            $data[$row->car_id] = $row->type;
        }


        return $data;
    }




    /*
     * Retrieve industries which allow the specified car
     * 
     * @param integer $car_id Car ID
     * @return array List of industry ids
     */

    public function get_industries($car_id)
    {
        $result = $this->get_many_by(['car_id' => $car_id]);

        // We return only a list of ids
        $data = [];
        foreach ($result as $row) {
            $data[] = $row->industry_id;
        }
        return $data;
    }


    /*
     * Save associations between a industry and allowed cars
     * @param int $industry_id Industry Id 
     * @param array $car_ids Array of car_id
     *
     * @return bool success of the query
     */
    public function save($industry_id, $car_ids)
    {

        // First, we remove previous associations
        $this->delete_by(['industry_id' => $industry_id]);

        // Then we save associations
        foreach ($car_ids as $car_id) {
            $data = [
                'industry_id' => $industry_id,
                'car_id' => $car_id,
            ];

            $result = $this->insert($data);
            //print_r($result);
        }

        //exit();
    }
}
