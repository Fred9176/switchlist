<?php

class Industry_spotted_car_model extends My_Model {

    //protected $return_type = 'array';

    private $_industries = [];

    /*
     * Get all spotted cars from database
     *
     * @return array carsid for all industry_ids
     */

    public function get_saved_spots() {

        $result = $this->get_all();
        //print_r($result);exit();
        $data = [];

        foreach ($result as $row) {
            $data[$row->industry_id][] = $row->car_id;
        }

        return $data;
    }

    /*
     * Spot car
     *
     * @param int $car_id Car I to spot
     *
     * @return int Location ID where to spot the car
     */

    public function spot_car($car_id, $not_in_industry_id = null) {

        // We choose a random industry
        // where the car is allowed
        // or where no allowed car is specified
        // and where there are free spots (for current spot and planned moves)
        // we exclude industries in the same location

        $this->load->model('industry_model', 'industry');
        $industry = $this->industry->get($not_in_industry_id);

        $where = '(a.car_id = ' . $car_id . ' OR a.car_id IS NULL)';
        if (is_numeric($not_in_industry_id)) {
            $where .= ' AND i.location_id <> ' . $industry->location_id;
        }


        $query = $this->_database->select('i.id')
                ->from('industries i')
                ->join('industry_allowed_cars a', 'i.id = a.industry_id', 'LEFT')
                ->join('industry_spotted_cars s', 'i.id = s.industry_id', 'LEFT')
                ->join('industry_spotted_cars m', 'i.id = m.move_to_industry_id', 'LEFT')
                ->where($where)
                ->group_by('i.id')
                ->having('(COUNT(s.car_id) + COUNT(m.car_id)) < i.size')
                ->order_by('RANDOM()')
                ->limit(1)
                ->get();

        //$this->utilities->debug($query);

        $result = $query->result();

        $industry_id = NULL;
        if (count($result) > 0) {
            $industry_id = $result[0]->id;
        }

        return $industry_id;
    }

    /*
     * Get a random number of spotted cars
     *
     * @param int $number Number of spotted cars to choose
     *
     * @return array spotted cars
     */

    public function get_random($number) {
        $result = $this->order_by('RANDOM()')
                ->limit($number)
                ->get_all();

        return $result;
    }

    /*
     * Get formatted datas to display the switchlist
     *
     * @return array Switchlist data
     */

    public function get_for_switchlist() {


        // Retrieve industries
        $this->load->model('industry_model', 'industry');
        //$result = $this->industry->order_by('order, name')->get_all();
        $result = $this->industry->get_industries_with_location();

        $industries = [];
        foreach ($result as $industry) {
            $industries[$industry->id] = [
                'name' => $industry->name,
                'location_name' => $industry->location_name,
                'location_id' => $industry->location_id,
                'pickup' => [],
                'drop' => []
            ];
        }

        $data = [];
        $data['Eastbound'] = $industries;
        $data['Westbound'] = array_reverse($industries, true);

        // We generate result for each train
        foreach ($this->config->item('trains') as $train) {

            $where = $train == 'Eastbound' ? '"loc_to"."order" >= "loc_from"."order"' : '"loc_to"."order" < "loc_from"."order"';

            $query = $this->_database->select("s.id, s.from_industry_id as ind_from_id, s.state, s.move_to_industry_id AS ind_to_id, s.car_id, c.type AS car_type, loc_from.name AS loc_from_name, ind_from.name AS ind_from_name, loc_to.name AS loc_to_name, ind_to.name AS ind_to_name ")
                    ->from($this->_table . ' s')
                    ->join('cars c', 'c.id = s.car_id', 'INNER')
                    ->join('industries ind_from', 'ind_from.id = s.from_industry_id', 'INNER')
                    ->join('locations loc_from', 'loc_from.id = ind_from.location_id', 'INNER')
                    ->join('industries ind_to', 'ind_to.id = s.move_to_industry_id', 'INNER')
                    ->join('locations loc_to', 'loc_to.id = ind_to.location_id', 'INNER')
                    ->where($where)
                    ->order_by('train, loc_from.order, loc_from.name, c.type')
                    ->get();
            //$this->utilities->debug($query);
            $result = $query->result();

            foreach ($result as $row) {
                $data[$train][$row->ind_from_id]['pickup'][$row->id] = ['car_state' => $row->state, 'car_type' => $row->car_type, 'move_to' => $row->loc_to_name . ' - ' . $row->ind_to_name];
                $data[$train][$row->ind_to_id]['drop'][$row->id] = ['car_state' => $row->state, 'car_type' => $row->car_type, 'from' => $row->loc_from_name . ' - ' . $row->ind_from_name];
            }
            //$this->utilities->debug($data[$train], 'result');
        }

        return $data;
    }

    /*
     * Get form result and convert it to spotted cars by industries
     *
     * @param array $spotted_cars Array of number of cars per industry
     *
     * @return array Cars per industries
     */

    public function convert_spotted_post($spotted_cars_post) {
        $spotted_cars = [];
        //        print_r($spotted_cars);
        //        exit;
        foreach ($spotted_cars_post as $industry_id => $cars) {

            $spotted_cars[$industry_id] = [];

            foreach ($cars as $car_id => $number) {
                // The POST result contains empty fields so we have to ckeck if a numercial value (ie number of cars) is entered
                if (is_numeric($number)) {
                    //$tmp_array[$industry_id][] = $car_id;
                    // Create item for each car

                    $tmp_array = $number > 0 ? array_fill(0, $number, $car_id) : [];

                    // Add items to the global cars'list
                    $spotted_cars[$industry_id] = array_merge($spotted_cars[$industry_id], $tmp_array);
                }
            }
        }

        return $spotted_cars;
    }

    /*
     * Save associations between a industry and allowed cars
     * @param array $spotted_cars Array of car_ids per industry_id
     *
     * @return bool success of the query
     */

    public function save_all($spotted_cars) {

        // Create array to insert
        $data = [];
        foreach ($spotted_cars as $industry_id => $cars) {

            foreach ($cars as $car_id) {
                $data[] = ['industry_id' => $industry_id, 'car_id' => $car_id];
            }
        }

        // Remove previous datas
        $this->delete_by('industry_id > 0');

        // Insert datas
        $result = $this->insert_many($data);

        // The result of insert return an array of inserted ids
        // which should be equal to the number of data to insert
        return (count($result) == count($data));
    }

}
