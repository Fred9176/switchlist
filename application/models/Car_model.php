<?php

class Car_model extends My_Model
{

    public $protected_attributes = ['id'];

    // Default value
    public $type = '';
    public $number = 0;


    // Validation rules
    public $validate = [
        [
            'field' => 'type',
            'label' => 'lang:type', //$this->lang->line('type'),
            'rules' => 'required'
        ],
        [
            'field' => 'number',
            'label' => 'lang:owned', //$this->lang->line('type'),
            'rules' => 'required|greater_than[0]'
        ],
    ];

    /*
     * Get total cars by summing car's number
     */
    public function count_total()
    {
        $result = $this->_database->select_sum('number')
            ->get($this->_table)
            ->result();

        return $result[0]->number;
    }


    /*
     * Get a set of random cars
     * 
     * @param int $number Number of wanted cars
     * 
     * @return array Arrya of car ids
     */
    function get_random_cars($number)
    {

        $cars = $this->get_all();

        // Flattening array (1 item per real car)
        $cars_list = [];
        foreach ($cars as $car) {

            // Create item for each car
            $tmp_array = array_fill(0, $car->number, $car->id);

            // Add items to the global cars'list
            $cars_list = array_merge($cars_list, $tmp_array);
        }

        // Shuffle the array
        shuffle($cars_list);

        // take only the specified number of cars
        $cars_list = array_slice($cars_list, 0, $number);

        return $cars_list;
    }
}
