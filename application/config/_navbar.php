<?php

defined('BASEPATH') or exit('No direct script access allowed');

$CI = &get_instance();
$CI->lang->load('switchlist');

$config['navbar_items'] = [
    /*'home' => [
        'link' => '',
        'icon' => 'home',
        'text' => ''
    ),*/
    'cars' => [
        'link' => 'car',
        'icon' => 'minecart-loaded',
        'text' => $CI->lang->line('nav_set_cars')
    ],
    'locations' => [
        'link' => 'location',
        'icon' => 'geo',
        'text' => $CI->lang->line('nav_set_locations')
    ],
    'industries' => [
        'link' => 'industry',
        'icon' => 'building',
        'text' => $CI->lang->line('nav_set_industries')
    ],
    'setup' => [
        'link' => 'setup',
        'icon' => 'bullseye',
        'text' => $CI->lang->line('nav_place_cars_in_industries')
    ],

    'switchlist' => [
        'link' => 'switchlist',
        'icon' => 'card-list',
        'text' => $CI->lang->line('nav_genereate_switchlist')
    ],
];
