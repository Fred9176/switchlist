<?php

// http://deercreation.fr/article/creer-systeme-de-layouts-avec-codeigniter
// http://openclassrooms.com/courses/codeigniter-le-framework-au-service-des-zeros/mise-en-place-des-layouts

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Layout {

    private $CI;
    private $layout = 'main';           // Default layout
    private $params = [];          // Parameters to set in layout

    /*
     * Constructor
     *
     * Set default global parameters
     */

    public function __construct() {

        $this->CI = &get_instance();

        $this->params['layout_css'] = '';
        $this->params['layout_js_head'] = '';
        $this->params['layout_js_bottom'] = '';
        $this->params['layout_navbar_active'] = '';
        $this->params['title'] = '';
    }

    /*
     * Set specific layout
     *
     * @param string $layout Set layout to use
     *
     * @return $this to allow methods chain
     */

    public function setLayout($layout) {
        $this->layout = $layout;
        return $this;
    }

    /**
     * Set navbar
     */
    public function setNavbar() {
        $this->params['navbar'] = $this->CI->load->view('_layout/navbar.php', ['navbar_active' => $this->params['layout_navbar_active']], TRUE);
    }

    /*
     * Set parameter
     *
     * @param string $param Name of the parameter to set
     * @param string $value Value of the parameter to set
     *
     * @return $this to allow methods chain
     */

    public function setParam($param, $value) {

        // Multiple params assignment are concatenated
        if ($this->params[$param] == '') {
            $this->params[$param] = $value;
        } else {
            $this->params[$param] .= $value;
        }
        return $this;
    }

    /*
     * Add view to the layout
     *
     * @param string $param Name of the parameter to set
     * @param string $name Name of te view to set
     * @param array $data Datas to set into the view
     *
     * @return $this to allow methods chain
     */

    public function setView($param, $name, $data = []) {

        $this->setNavbar();
        $this->params[$param] = $this->CI->load->view($name, $data, true);

        return $this;
    }

    /*
     * Add css file
     *
     * @param string $filename Set css filename to use without extension
     *
     * @return $this to allow methods chain
     */

    public function add_css($filename, $url = FALSE) {
        if (!$url) {
            $filename = 'assets/css/' . $filename . '.css';
        }

        $this->setParam('layout_css', link_tag($filename));
        return $this;
    }

    /*
     * Add js file
     *
     * @param string $filename Set script filename to use without extension
     * @param string $location Define where the script must be located (header or bottom)
     *
     * @return $this to allow methods chain
     */

    public function add_js($filename, $location = 'head', $url = FALSE) {

        if (!$url) {
            $filename = base_url('assets/js/' . $filename . '.js');
        }

        $this->setParam('layout_js_' . $location, '<script type="text/javascript" src="' . $filename . '"></script>');
        return $this;
    }

    /*
     * Render global view
     */

    public function render() {
        $this->CI->load->view('_layout/' . $this->layout, $this->params);
    }

}
