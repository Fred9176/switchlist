<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Utilities {
        private $CI;

    /*
     * Constructor
     * 
     * Set default global parameters 
     */

    public function __construct() {
          
        $this->CI = & get_instance();

    }
    
  
    /*
     * Create a session flashdata message
     * 
     * @param string $message Message to display
     * @param string $state State of the message (success, info, alert, dander)
     */
    public function set_flashdata($message, $state = 'success'){
        $this->CI->session->set_flashdata('message', $message);
        $this->CI->session->set_flashdata('state', $state);
    }
    
  
    /*
     * Display a session flashdata message
     * 
     * @param string $message Message to display
     * @param string $state State of the message (success, info, alert, dander)
     */
    public function display_flashdata(){
        echo bootstrap_alert($this->CI->session->flashdata("message"), $this->CI->session->flashdata("state"));
    }   
    
    
    /*
     * Display a debug message
     * 
     * @param string $data Data to display
     * @param string $name Name of the debug info
     * @param bool $exit Exit after display
     */
    public function debug($data, $name = '', $exit = TRUE){
        echo '<pre style="margin-top: 50px">';
        echo '<hr/>';
        echo $name != '' ? '<b>'.$name.'</b><br/>' : '';
        print_r($data);
        echo '</pre>';
        
        if($exit){
            exit();
        }
        
    }
}
