<?php

# Navbar
$lang['nav_set_cars'] = '1. Set cars';
$lang['nav_set_locations'] = '2. Set locations';
$lang['nav_set_industries'] = '3. Set industries';
$lang['nav_place_cars_in_industries'] = '4. Place cars in locations';
$lang['nav_genereate_switchlist'] = '5. Generate switchlist';

# Cars
$lang['car_add_new'] = 'Add a new car';
$lang['car_edit'] = 'Edit car "%s"';
$lang['cars'] = 'Cars';
$lang['car_created_successfully'] = 'Car successfully created';
$lang['car_updated_successfully'] = 'Car "%s" successfully updated';
$lang['car_deleted_successfully'] = 'Car successfully deleted';
$lang['allowed_cars'] = 'Allowed cars';

# Locations
$lang['location_add_new'] = 'Add a new location';
$lang['location_edit'] = 'Edit location "%s"';
$lang['location'] = 'Location';
$lang['locations'] = 'Locations';
$lang['location_information'] = 'Informations on location';
$lang['location_created_successfully'] = 'Location successfully created';
$lang['location_updated_successfully'] = 'Location "%s" successfully updated';
$lang['location_deleted_successfully'] = 'Location successfully deleted';

# Industries
$lang['industry_add_new'] = 'Add a new industry';
$lang['industry_edit'] = 'Edit industry "%s"';
$lang['industry'] = 'Industry';
$lang['industries'] = 'Industries';
$lang['industry_information'] = 'Informations on industry';
$lang['industry_created_successfully'] = 'Industry successfully created';
$lang['industry_updated_successfully'] = 'Industry "%s" successfully updated';
$lang['industry_deleted_successfully'] = 'Industry successfully deleted';

# Setup
$lang['setup_number_of_cars_to_spot'] = 'Number of cars to spot (%d max)';
$lang['setup_generate_random_spots'] = 'Generate spots';
$lang['setup_cars_spotted_successfully'] = 'Cars successfully spoted';

# Switchlist
$lang['switchlist'] = 'Switchlist';
$lang['switchlist_number_of_cars_to_move'] = 'Number of cars to move (%d actually spotted)';
$lang['switchlist_generate_new_moves'] = 'Generate new moves';
$lang['switchlist_please_select_a_number_of_cars_to_move_above'] = 'Select a number of cars to  move above';
$lang['switchlist_cars_moves_generated_successfully'] = 'Moves successfully generated';
$lang['pickup'] = 'Pick up %s';
$lang['drop'] = 'Drop %s';

# Generic
$lang['type'] = 'Type';
$lang['number'] = 'Number';
$lang['name'] = 'Name';
$lang['order'] = 'Order';
$lang['size'] = 'Size';

$lang['from'] = 'From';
$lang['to'] = 'To';

$lang['action'] = 'Action';
$lang['actions'] = 'Actions';
$lang['setup'] = 'Setup';
$lang['owned'] = 'Owned';
$lang['edit'] = 'Editer';
$lang['update'] = 'Update';
$lang['add'] = 'Add';
$lang['save'] = 'Save';
$lang['are_you_sure'] = 'Are you sure ?';
$lang['all'] = 'All';
