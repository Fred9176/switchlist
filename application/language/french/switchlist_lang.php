<?php

# Navbar
$lang['nav_set_cars'] = '1. Gérer les wagons';
$lang['nav_set_locations'] = '2. Gérer les localités';
$lang['nav_set_industries'] = '3. Gérer les industries';
$lang['nav_place_cars_in_industries'] = '4. Placer les wagons sur les industries';
$lang['nav_genereate_switchlist'] = '5. Générer une switchlist';

# Cars
$lang['car_add_new'] = 'Ajouter un nouveau wagon';
$lang['car_edit'] = 'Editer le wagon "%s"';
$lang['cars'] = 'Wagons';
$lang['car_created_successfully'] = 'Le wagon a été créé avec succès.';
$lang['car_updated_successfully'] = 'Le wagon "%s" a été modifié avec succès.';
$lang['car_deleted_successfully'] = 'Le wagon a été supprimé avec succès.';
$lang['allowed_cars'] = 'Wagons autorisés';

# Locations
$lang['location_add_new'] = 'Ajouter une nouvelle localité';
$lang['location_edit'] = 'Editer la localité "%s"';
$lang['location'] = 'Localité';
$lang['locations'] = 'Localités';
$lang['location_information'] = 'Informations sur la localité';
$lang['location_created_successfully'] = 'La localité a été créée avec succès.';
$lang['location_updated_successfully'] = 'La localité "%s" a été modifiée avec succès.';
$lang['location_deleted_successfully'] = 'La localité a été supprimée avec succès.';

# Industries
$lang['industry_add_new'] = 'Ajouter une nouvelle industrie';
$lang['industry_edit'] = 'Editer l\'industrie "%s"';
$lang['industry'] = 'Industrie';
$lang['industries'] = 'Industries';
$lang['industry_information'] = 'Informations sur l\'industrie';
$lang['industry_created_successfully'] = 'L\'industrie a été créée avec succès.';
$lang['industry_updated_successfully'] = 'L\'industrie "%s" a été modifiée avec succès.';
$lang['industry_deleted_successfully'] = 'L\'industrie a été supprimée avec succès.';

# Setup
$lang['setup_number_of_cars_to_spot'] = 'Nombre de wagons à placer (%d max)';
$lang['setup_generate_random_spots'] = 'Générer les emplacements';
$lang['setup_cars_spotted_successfully'] = 'Wagons positionnés avec succès';

# Switchlist
$lang['switchlist'] = 'Switchlist';
$lang['switchlist_number_of_cars_to_move'] = 'Nombre de wagons à déplacer (%d actuellement placés)';
$lang['switchlist_generate_new_moves'] = 'Générer de nouveaux mouvements';
$lang['switchlist_please_select_a_number_of_cars_to_move_above'] = 'Sélectionner un nombre de wagons à déplacer ci-dessus';
$lang['switchlist_cars_moves_generated_successfully'] = 'Déplacements générés avec succès';
$lang['pickup'] = 'Prendre %s';
$lang['drop'] = 'Déposer %s';

# Generic
$lang['type'] = 'Type';
$lang['number'] = 'Nombre';
$lang['name'] = 'Nom';
$lang['order'] = 'Ordre';
$lang['size'] = 'Contenance';

$lang['from'] = 'En provenance de';
$lang['to'] = 'Vers';

$lang['action'] = 'Action';
$lang['actions'] = 'Actions';
$lang['setup'] = 'Paramétrage';
$lang['owned'] = 'Possédés';
$lang['edit'] = 'Editer';
$lang['update'] = 'Mettre à jour';
$lang['add'] = 'Ajouter';
$lang['save'] = 'Sauvegarder';
$lang['are_you_sure'] = 'Etes-vous sûr ?';
$lang['all'] = 'Tous';
