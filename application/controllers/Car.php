<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Car extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('car_model', 'car');
    }

    /*
     * List of items
     */

    function index($page = 1, $status = '') {

        // Generate pagination
        $this->load->library('pagination');
        $pagination = bootstrap_pagination($this->pagination, [
            'base_url' => site_url('car'),
            'total' => $this->car->count_all(),
            'page' => $page,
        ]);

        // Get Cars list
        $data['cars'] = $this->car->limit($pagination['limit'], $pagination['offset'])->order_by('type')->get_all();

        $data['pagination'] = $pagination['links'];

        // Display list
        $this->load->library('layout');
        $this->layout
                ->setParam('layout_navbar_active', 'cars')
                ->setParam('title', $this->lang->line('cars'))
                ->setView('layout_content', 'car/list', $data)
                ->render();
    }

    /*
     * Add new item
     */

    function create() {

        $car = $this->car;

        // If post has been submitted
        if ($this->input->post('create_car')) {
            $data = $this->input->post('car');

            // We try to save item using form validation
            if ($this->car->insert($data)) {

                // On success => return to list
                $this->utilities->set_flashdata($this->lang->line('car_created_successfully'), 'success');
                redirect('car');
            } else {

                // On failed => return to form with new values
                $car->type = $data['type'];
                $car->number = $data['number'];

                $this->utilities->set_flashdata(validation_errors(), 'danger');
            }
        }


        $data['car'] = $car;
        $info['title'] = 'Create new car';

        $this->load->library('layout');
        $this->layout
                ->setParam('layout_navbar_active', 'cars')
                ->setParam('title', $this->lang->line('car_add_new'))
                ->setView('layout_content', "car/create", array_merge($data, $info))
                ->render();
    }

    function edit() {

        //Retrieve asked item
        $id = $this->uri->segment(3);
        $car = $this->car->get($id);
        if (!$car) {
            redirect('car');
        }

        // If form has been submitted
        if ($this->input->post('update_car')) {
            $data = $this->input->post('car');

            // We try to save item using form validation
            if ($this->car->update($id, $data)) {

                // On success => return to list
                $this->utilities->set_flashdata($this->lang->line('car_updated_successfully', [$car->type]), 'success');
                redirect('car');
            } else {

                // On failed => return to form with new values
                $car->type = $data['type'];
                $car->number = $data['number'];

                $this->utilities->set_flashdata(validation_errors(), 'danger');
            }
        }


        $data['car'] = $car;
        $info['title'] = 'Edit car :' . $car->type;

        $this->load->library('layout');
        $this->layout
                ->setParam('layout_navbar_active', 'cars')
                ->setParam('title', $this->lang->line('car_edit', [$car->type]))
                ->setView('layout_content', "car/edit", array_merge($data, $info))
                ->render();
    }

    function delete() {
        $id = $this->uri->segment(3);
        $this->car->delete($id);

        $this->load->model('Location_allowed_car_model', 'allowed_car');
        $this->load->model('Location_spotted_car_model', 'spotted_car');

        $this->allowed_car->delete_by(['car_id' => $id]);
        $this->spotted_car->delete_by(['car_id' => $id]);

        $this->utilities->set_flashdata($this->lang->line('car_deleted_successfully'), 'success');
        redirect("car");
    }

}
