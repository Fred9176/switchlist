<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Switchlist extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Industry_spotted_car_model', 'spotted_car');
    }

    /*
     * Index Page : Get cars position from database
     */

    public function index() {

        // retrieve datas
        $data['trains'] = $this->spotted_car->get_for_switchlist();
        $data['spotted_cars_count'] = $this->spotted_car->count_all();
        $data['cars_to_move_count'] = $this->spotted_car->count_by('move_to_industry_id > 0');

        $this->load->library('layout');

        $this->layout
                ->setParam('layout_navbar_active', 'switchlist')
                ->setParam('title', $this->lang->line('switchlist'))
                ->setView('layout_content', 'switchlist/switchlist', $data)
                ->render();
    }

    /*
     * View Page : show switchlist in vintage sheet
     */

    public function vintage() {

        // Calcule Date to diplay
        $data['date'] = [
            'day' => date('M, j'),
            'year' => date('y'),
        ];

        // retrieve datas
        $data['trains'] = $this->spotted_car->get_for_switchlist();
        $data['spotted_cars_count'] = $this->spotted_car->count_all();
        $data['cars_to_move_count'] = $this->spotted_car->count_by('move_to_industry_id > 0');

        $this->load->view('switchlist/vintage', $data);
    }

    /*
     * Generate a list of moves
     */

    public function generate() {


        // Clear all previous moves
        $this->spotted_car->update_all(['from_industry_id' => NULL, 'move_to_industry_id' => NULL, 'state' => NULL]);

        // Get number of cars to move
        $cars_to_move = $this->spotted_car->get_random($this->input->post('number_to_move'));

        // Find a free spot for each car
        foreach ($cars_to_move as $spotted_car) {
            $move_to_industry_id = $this->spotted_car->spot_car($spotted_car->car_id, $spotted_car->industry_id);
            //$this->utilities->debug($move_to_industry_id, 'move_to_industry_id', FALSE);

            $this->spotted_car->update($spotted_car->id, ['from_industry_id' => $spotted_car->industry_id, 'move_to_industry_id' => $move_to_industry_id]);
        }

        // Return to switchlist
        $this->utilities->set_flashdata($this->lang->line('switchlist_cars_moves_generated_successfully'), 'success');
        redirect('switchlist');
    }

    /*
     * Save moves from Ajax query
     */

    public function save_move() {

        $id = $this->input->post('id');
        $pickup = $this->input->post('pickup') == 'true' ? TRUE : FALSE;
        $drop = $this->input->post('drop') == 'true' ? TRUE : FALSE;

        // Get current car
        $car = $this->spotted_car->get($id);

        // If car is dropped (and whatever the pickup checkbox state is)
        // We place it in the new industry
        if ($drop) {
            $state = 'dropped';
            $industry_id = $car->move_to_industry_id;
        }

        // If car is picked up and not dropped yet
        // the car is on its way and is not anymore in the origin industry
        // and not yet in the destination industry
        else if ($pickup && !$drop) {
            $state = 'pickedup';
            $industry_id = NULL;
        }

        // Unknow state (generally cancelled move) => set back to origin industry
        else {
            $state = NULL;
            $industry_id = $car->from_industry_id;
        }

        // Save move state
        $this->spotted_car->update($id, ['industry_id' => $industry_id, 'state' => $state]);

        // debug display
        $data = [
            'id' => $id,
            'pickup' => $pickup,
            'drop' => $drop,
            'state' => $state,
        ];
        echo json_encode($data);
    }

}
