<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Setup extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('industry_model', 'industry');
        $this->load->model('car_model', 'car');
        $this->load->model('Industry_allowed_car_model', 'allowed_car');
        $this->load->model('Industry_spotted_car_model', 'spotted_car');
    }

    /*
     * Index Page : Get cars position from database
     */

    public function index()
    {

        // retrieve datas
        $data['industries'] = $this->industry->get_industries_with_location();

        $data['cars'] = $this->car->order_by('type')->get_all();
        $data['cars_count'] = $this->car->count_total();


        // Save spotted cars
        if ($this->input->post('save')) {

            $data['spotted_cars'] = $this->spotted_car->convert_spotted_post($this->input->post('spotted_cars'));
            $this->spotted_car->save_all($data['spotted_cars']);

            // On success => return to the setup page
            $this->utilities->set_flashdata($this->lang->line('setup_cars_spotted_successfully'), 'success');
            redirect('setup');
        }


        // Generate random spotted cars if corresponding form is submitted
        elseif ($this->input->post('get_random')) {

            //Clear already spotted cars (because they are used in searching free spots)
            $this->spotted_car->truncate();

            $number = $this->input->post('number_to_spot');

            $cars = $this->car->get_random_cars($number);
            $data['spotted_cars'] = [];

            foreach ($cars as $car_id) {
                $industry_id = $this->spotted_car->spot_car($car_id);

                if (!is_null($industry_id)) {
                    $data['spotted_cars'][$industry_id][] = $car_id;
                }
            }
        }


        // By default, retrieve spotted cars from database
        else {

            $data['spotted_cars'] = $this->spotted_car->get_saved_spots();
        }

        $this->load->library('layout');

        $this->layout
            ->setParam('layout_navbar_active', 'setup')
            ->setParam('title', $this->lang->line('setup'))
            //->add_css('https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.css', TRUE)
            //->add_css('https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/sticky-header/bootstrap-table-sticky-header.css', TRUE)
            //->add_js('https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.js', 'bottom', TRUE)
            //->add_js('https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/sticky-header/bootstrap-table-sticky-header.min.js', 'bottom', TRUE)
            ->add_js('bootstrap-input-spinner', 'bottom')
            //->add_js('tableStickyHeader', 'bottom')
            ->setView('layout_content', 'setup/grid', $data)
            ->render();
    }
}
