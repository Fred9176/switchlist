<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Location extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('location_model', 'location');
    }




    /*
     * List of items
     */
    function index($page = 1, $status = '')
    {

        // Generate pagination
        $this->load->library('pagination');
        $pagination = bootstrap_pagination(
            $this->pagination,
            [
                'base_url' => site_url('location'),
                'total' => $this->location->count_all(),
                'page' => $page,
            ]
        );

        // Get locations list
        $data['locations'] = $this->location->limit($pagination['limit'], $pagination['offset'])->order_by('order, name')->get_all();



        $data['pagination'] = $pagination['links'];

        // Display list
        $this->load->library('layout');
        $this->layout
            ->setParam('layout_navbar_active', 'locations')
            ->setParam('title', $this->lang->line('locations'))
            ->setView('layout_content', 'location/list', $data)
            ->render();
    }




    /*
     * Add new item
     */
    function create()
    {

        $location = $this->location;

        // If post has been submitted
        if ($this->input->post('create')) {
            $data = $this->input->post('location');


            // We try to save item using form validation
            $resultId = $this->location->insert($data);

            if ($resultId !== false) {
                // On success => return to list
                $this->utilities->set_flashdata($this->lang->line('location_created_successfully'), 'success');
                redirect('location');
            } else {

                // On failed => return to form with new values
                $location->name = $data['name'];
                $location->order = $data['order'];
                $location->size = $data['size'];

                $this->utilities->set_flashdata(validation_errors(), 'danger');
            }
        }



        $data['location'] = $location;
        $info['title'] = 'Create new location';

        $this->load->library('layout');
        $this->layout
            ->setParam('layout_navbar_active', 'locations')
            ->setParam('title', $this->lang->line('location_add_new'))
            ->setView('layout_content', "location/create", array_merge($data, $info))
            ->render();
    }




    /*
     * Edit item
     */
    function edit()
    {

        //Retrieve asked item
        $id = $this->uri->segment(3);
        $location = $this->location->get($id);


        if (!$location) {
            redirect('location');
        }

        // If form has been submitted
        if ($this->input->post('update')) {
            $locationData = $this->input->post('location');

            // We try to save item using form validation
            if ($this->location->update($id, $locationData)) {

                // On success => return to list
                $this->utilities->set_flashdata($this->lang->line('location_updated_successfully', [$location->name]), 'success');
                redirect('location');
            } else {

                // On failed => return to form with new values
                $location->name = $locationData['name'];
                $location->order = $locationData['order'];

                $this->utilities->set_flashdata(validation_errors(), 'danger');
            }
        }




        $data['location'] = $location;
        $info = [];

        $this->load->library('layout');
        $this->layout
            ->setParam('layout_navbar_active', 'locations')
            ->setParam('title', $this->lang->line('location_edit', [$location->name]))
            ->setView('layout_content', "location/edit", array_merge($data, $info))
            ->render();
    }

    function delete()
    {
        $id = $this->uri->segment(3);

        $this->location->delete($id);

        $this->utilities->set_flashdata($this->lang->line('location_deleted_successfully'), 'success');
        redirect("location");
    }
}
