<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Industry extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('industry_model', 'industry');
        $this->load->model('location_model', 'location');
        $this->load->model('car_model', 'car');
        $this->load->model('Industry_allowed_car_model', 'allowed_car');
        $this->load->model('Industry_spotted_car_model', 'spotted_car');
    }

    /*
     * List of items
     */

    function index($page = 1)
    {

        // Generate pagination
        $this->load->library('pagination');
        $pagination = bootstrap_pagination(
            $this->pagination,
            [
                'base_url' => site_url('industry'),
                'total' => $this->industry->count_all(),
                'page' => $page,
            ]
        );

        // Get industries list
        //$data['industries'] = $this->industry->with('location')->limit($pagination['limit'], $pagination['offset'])->order_by('size')->get_all();

        $params = [];
        $params['pagination']['limit'] = $pagination['limit'];
        $params['pagination']['offset'] = $pagination['offset'];

        $data['industries'] = $this->industry->get_industries_with_location($params);


        // Retrieve allowed cars for each industry
        $industriesCount = count($data['industries']);
        for ($i = 0; $i < $industriesCount; $i++) {
            $data['industries'][$i]->allowed_cars = $this->allowed_car->get_cars($data['industries'][$i]->id);
        }
        // print_r($data['industries']);
        // exit();


        $data['pagination'] = $pagination['links'];

        // Display list
        $this->load->library('layout');
        $this->layout
            ->setParam('layout_navbar_active', 'industries')
            ->setParam('title', $this->lang->line('industries'))
            ->setView('layout_content', 'industry/list', $data)
            ->render();
    }

    /*
     * Add new item
     */

    function create()
    {

        $industry = $this->industry;

        // If post has been submitted
        if ($this->input->post('create')) {
            $data = $this->input->post('industry');
            $allowedCars = $this->input->post('allowedCars');


            // We try to save item using form validation
            $resultId = $this->industry->insert($data);
            if ($resultId !== false) {


                // Update allowed cars
                $this->allowed_car->save($resultId, $allowedCars);


                // On success => return to list
                $this->utilities->set_flashdata($this->lang->line('industry_created_successfully'), 'success');
                redirect('industry');
            } else {

                // On failed => return to form with new values
                $industry->name = $data['name'];
                $industry->location_id = $data['location_id'];
                $industry->size = $data['size'];

                $data['allowedCars'] = $allowedCars;

                $this->utilities->set_flashdata(validation_errors(), 'danger');
            }
        } else {
            $data['allowedCars'] = [];
        }


        // Retrive cars list
        $data['cars'] = $this->car->order_by('type')->get_all();

        //Retrieve locations list
        $data['locations'] = $this->location->prepare_dropdown();

        $data['industry'] = $industry;
        $info['title'] = 'Create new industry';

        $this->load->library('layout');
        $this->layout
            ->setParam('layout_navbar_active', 'industries')
            ->setParam('title', $this->lang->line('industry_add_new'))
            ->setView('layout_content', "industry/create", array_merge($data, $info))
            ->render();
    }

    /*
     * Edit item
     */

    function edit()
    {

        //Retrieve asked item
        $id = $this->uri->segment(3);
        $industry = $this->industry->with('allowed_cars')->get($id);


        if (!$industry) {
            redirect('industry');
        }

        // If form has been submitted
        if ($this->input->post('update')) {
            $industryData = $this->input->post('industry');
            $data['allowedCars'] = $this->input->post('allowedCars');

            // We try to save item using form validation
            if ($this->industry->update($id, $industryData)) {

                // Update allowed cars
                $this->allowed_car->save($id, $data['allowedCars']);

                // On success => return to list
                $this->utilities->set_flashdata($this->lang->line('industry_updated_successfully', [$industry->name]), 'success');
                redirect('industry');
            } else {

                // On failed => return to form with new values
                $industry->name = $industryData['name'];
                $industry->location_id = $industryData['location_id'];
                $industry->size = $industryData['size'];

                $this->utilities->set_flashdata(validation_errors(), 'danger');
            }
        } else {
            // Retrieve allowed cars from database
            $data['allowedCars'] = $this->allowed_car->get_car_ids($id);
        }



        // Retrive cars list
        $data['cars'] = $this->car->order_by('type')->get_all();

        //Retrieve locations list
        $data['locations'] = $this->location->prepare_dropdown();


        $data['industry'] = $industry;
        $info['title'] = 'Edit industry :' . $industry->name;

        $this->load->library('layout');
        $this->layout
            ->setParam('layout_navbar_active', 'industries')
            ->setParam('title', $this->lang->line('industry_edit', [$industry->name]))
            ->setView('layout_content', "industry/edit", array_merge($data, $info))
            ->render();
    }

    function delete()
    {
        $id = $this->uri->segment(3);

        $this->industry->delete($id);

        $this->allowed_car->delete_by(['industry_id' => $id]);
        $this->spotted_car->delete_by(['industry_id' => $id]);

        $this->utilities->set_flashdata($this->lang->line('industry_deleted_successfully'), 'success');
        redirect("industry");
    }
}
