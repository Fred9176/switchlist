// https://www.cssscript.com/demo/number-spinner-control-increment-stepper/

var inc = document.getElementsByClassName("stepper");
for (i = 0; i < inc.length; i++) {
    var incI = inc[i].querySelector("input"),
        id = incI.getAttribute("id"),
        min = incI.getAttribute("min"),
        max = incI.getAttribute("max"),
        step = incI.getAttribute("step");
    document
        .getElementById(id)
        .previousElementSibling.setAttribute(
            "onclick",
            "stepperInput(event, '" + id + "', -" + step + ", " + min + ")"
        );
    document
        .getElementById(id)
        .nextElementSibling.setAttribute(
            "onclick",
            "stepperInput(event, '" + id + "', " + step + ", " + max + ")"
        );
}

function stepperInput(e, id, s, m) {

    var el = document.getElementById(id);
    // console.log(id);
    // console.log(el.value);
    if (el.value == '') {
        el.value = 0;
    }


    if (s > 0) {
        if (parseInt(el.value) < m) {
            el.value = parseInt(el.value) + s;
        }
    } else {
        if (parseInt(el.value) > m) {
            el.value = parseInt(el.value) + s;
        }
    }

    if (el.value == 0) {
        //console.log('zero');
        el.value = '';
        //console.log(el.value);
    }

    e.preventDefault();
}