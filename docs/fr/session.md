# Session de jeu


## 4. Placer les wagons sur les industries

Sur cet écran, on peut soit :
* Demander au logiciel de nous placer un nombre défini de types de wagons de façon aléatoire
* Placer manuellement les types de wagons (si ils sont déjà placés sur le réseau par exemple)

![Placement des wagons](medias/4-placement.png)

## 5. Générer une switchlist

On demande au logiciel de générer aléatoirement les mouvements pour un certain nombre de wagons que l'on a placé précédemment.
Un mouvement correspond à un point de départ (pickup, là où le wagon est actuellement placé) et un point de livraison (drop) qui peut être soit à l'aller, soit au retour.

![Switchlist](medias/5-switchlist.png)

## Au cours de la session

Sur l'écran 5,  il suffit d'effectuer pour de vrai les mouvements indiqués, et dès qu'il est fait, de cocher la case correspondante (on peut aussi imprimer la page).
Le fait de cocher la case permet :
* de reprendre la session ultérieurement
* de modifier automatique l'emplacement du wagon (sur l'écran 4), pour une future session.

Un clic sur Vintage List affiche une vue plus en rapport avec une époque ancienne.
![Switchlist](medias/5-vintageswitchlist.png)
