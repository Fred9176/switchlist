
# Installation

## Méthode simple sous Windows

### Prérequis

* Récupérer la version **portable** de XAMPP (PHP 7.3) : https://sourceforge.net/projects/xampp/files/XAMPP%20Windows/
* Récupérer les sources depuis https://bitbucket.org/Fred9176/switchlist/src/master/
  (en haut à droite, cliquer sur ..., puis Download repository)

### Installation

* Dézipper le fichier zip de XAMPP dans C: (les fichiers doivent être dans un dossier C:\xampp)
* Supprimer tout le contenu du dossier C:\xampp\htdocs
* Dézipper les sources de switchlist dans C:\xampp\htdocs

### Lancement

* Lancer le fichier c:\xampp\xampp_start.exe (la 1ere fois, autoriser les demandes du pare-feu Windows)
* Ouvrir un navigateur et appeler l'url http://localhost


### Accès depuis une autre machine