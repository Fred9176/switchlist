# Configurer et utiliser Switchlist

## Switchlist : qu'est-ce-que c'est ?

Switchlist est une application qui permet de générer des switchlist (dingue non) pour le modélisme ferroviaire.
Une switchlist est un ensemble de mouvements de wagons que doit effectuer l'opérateur à l'aide de sa locmotive.

Ce logiciel est avant tout concçu pour mon réseau, mais peut être sans problème utilisé ailleurs.

Il y a quelques prérequis à comprendre pour l'utiliser :
* Il est conçu pour l'exploitation d'une ligne à voie unique, sur laquelle le train efectue un aller et un retour.
  On génère donc 2 switchlists : une pour l'aller et une pour le retour
* On déplace des types de wagon et non des wagons eux-même. Sur un petit réseau avec une dizaine de mouvements, cela ne pose pas de problème car .
* Les mouvements sont générés aléatoirement par le logiciel

Mon objectif était avant tout le jeu, pas le réalisme.

## Utiliser le logiciel

* [Installation](./install.md)
* [Paramétrage initial](./parametrage.md)
* [Session de jeu](./session.md)