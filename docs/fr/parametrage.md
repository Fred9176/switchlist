# Paramétrage initial

Cette section permet de définir à quoi ressemble le réseau dans le logiciel.


## 1. Gérer les wagons
On définit ici les types de wagons existants et le nombre que l'on possède pour chacun.

![Wagons](medias/1-wagons.png)

## 2. Gérer les localités
On définit ici les localités, dans l'ordre dans lequel le train les traverse à l'allée(chez moi, c'est Whiteleaf, Aylesbury, Wymore,....)

![Localités](medias/2-localites.png)

## 3. Gérer les industries
On définit ici les industries présentes dans chaque localité.

Pour chacune, on renseigne la contenance (nombre de wagons max) et le(s) type(s) de wagon(s) accepté(s) (si on ne renseigne rien, tous les types sont acceptés)

![Industries](medias/3-industries.png)
